import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import Typography from "@material-ui/core/Typography";

function preventDefault(event) {
    event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
}));

export default function Orders({allCustomers}) {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Title>Recent Orders</Title>
            {
                allCustomers.map(x => (
                    <div style={{marginBottom: '20px'}}>
                        <Typography style={{marginRight: '10px'}} component="subtitle1" variant="subtitle1" color="primary" gutterBottom>
                            Name: {x.name}
                        </Typography>
                        <Typography style={{marginRight: '10px'}} component="subtitle1" variant="subtitle1" color="primary" gutterBottom>
                            Email:  {x.email}
                        </Typography>
                        <Typography style={{marginRight: '10px', display: 'block'}} component="subtitle1" variant="subtitle1" color="primary" gutterBottom>
                            Accounts
                        </Typography>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>number</TableCell>
                                    <TableCell>currency</TableCell>
                                    <TableCell>balance</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {x.accounts.map((row) => (
                                    <TableRow key={row.id}>
                                        <TableCell>{row.number}</TableCell>
                                        <TableCell>{row.currency}</TableCell>
                                        <TableCell>{row.balance}</TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </div>
                ))
            }

            <div className={classes.seeMore}>
            <Link color="primary" href="#" onClick={preventDefault}>
                See more orders
            </Link>
            </div>
        </React.Fragment>
    );
}