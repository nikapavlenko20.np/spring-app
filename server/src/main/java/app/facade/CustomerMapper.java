package app.facade;

import app.dto.request.AccountRequest;
import app.dto.request.CustomerRequest;
import app.dto.response.AccountResponse;
import app.dto.response.CustomerResponse;
import app.emun.Currency;
import app.entity.Account;
import app.entity.Customer;
import app.entity.Employer;
import app.service.AccountService;
import app.service.CustomerService;
import app.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class CustomerMapper implements Facade<Customer, CustomerResponse, CustomerRequest> {
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    CustomerService customerService;
    @Autowired
    AccountService accountService;
    @Autowired
    EmployerService employerService;

    @Override
    public CustomerResponse toDTOResponse(CustomerRequest customerRequest) {
        return mapper.map(customerRequest, CustomerResponse.class);
    }

    @Override
    public CustomerResponse fromEntity(Customer customer) {
        return mapper.map(customer, CustomerResponse.class);
    }

    @Override
    public Customer toEntity(CustomerRequest customerRequest) {
        return mapper.map(customerRequest, Customer.class);
    }

    @Override
    public CustomerRequest toDTORequest(CustomerResponse customerResponse) {
        return mapper.map(customerResponse, CustomerRequest.class);
    }


    public Customer createCustomer(CustomerRequest customerRequest) {
        Customer customer = toEntity(customerRequest);
        return customerService.createCustomer(customer);
    }

    public List<CustomerResponse> getAllCustomers() {
        return customerService
                .getCustomers()
                .stream()
                .map(this::fromEntity)
                .collect(Collectors.toList());
    }

    public Customer updateCustomer(CustomerRequest customerRequest, Long id) {
        Customer customer = customerService.getCustomerById(id);
        customer.setName(customerRequest.getName());
        customer.setPhone(customerRequest.getPhone());
        customer.setPassword(customerRequest.getPassword());
        customer.setEmail(customerRequest.getEmail());
        customer.setAge(customerRequest.getAge());
        return customerService.updateCustomer(customer);
    }

    public CustomerResponse getCustomerById(long id) {
        Customer customer = customerService.getCustomerById(id);
        return fromEntity(customer);
    }

    public boolean deleteCustomer(long id) {
       return customerService.deleteCustomer(id);
    }

    public Account createAccount(String currency, long id) {
        Customer customer = customerService.getCustomerById(id);
        Account account = new Account();
        account.setCurrency(Currency.getByName(currency));
        account.setCustomer(customer);

        customer.getAccounts().add(account);
        return accountService.saveAccount(account);
    }

    public void setEmployer(long customerID, long employerId) {
        Customer customer = customerService.getCustomerById(customerID);
        Employer employer = employerService.getEmployerById(employerId);
        customer.getEmployers().add(employer);
        customerService.createCustomer(customer);
        employerService.createEmploye(employer);
    }
}
