package app.facade;

import app.dto.request.AccountRequest;
import app.dto.response.AccountResponse;
import app.entity.Account;
import app.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class AccountMapper implements Facade<Account, AccountResponse, AccountRequest>{
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    AccountService accountService;

    @Override
    public AccountResponse toDTOResponse(AccountRequest accountRequest) {
        return mapper.map(accountRequest, AccountResponse.class);
    }

    @Override
    public AccountRequest toDTORequest(AccountResponse accountResponse) {
        return mapper.map(accountResponse, AccountRequest.class);
    }

    @Override
    public AccountResponse fromEntity(Account entity) {
        return mapper.map(entity, AccountResponse.class);
    }

    @Override
    public Account toEntity(AccountRequest accountRequest) {
        return mapper.map(accountRequest, Account.class);
    }

    public void deleteAccount(Long id) {
        Optional<Account> account = accountService.getAccountById(id);
        if (account.isPresent()) {
            accountService.deleteAccountById(id);
        }
    }

    public void topUpAccount(Double money, Long id) {
        Optional<Account> account = accountService.getAccountById(id);
        if (account.isPresent()) {
            Account a = account.get();
            a.setBalance(a.getBalance() + money);
            accountService.saveAccount(a);
        }
    }

    public boolean cashOutForAccount(Double money, long accountID) {
        Optional<Account> account = accountService.getAccountById(accountID);
        if (account.isPresent() && account.get().getBalance() > money) {
            Account a = account.get();
            a.setBalance(a.getBalance() - money);
            accountService.saveAccount(a);
            return true;
        }
        return false;
    }

    public boolean transactionForAccounts(Double money, long accountFromID, long accountToID) {
        boolean moneyForAccount = cashOutForAccount(money, accountFromID);
        if (moneyForAccount) {
            topUpAccount(money, accountToID);
            return true;
        }
        return false;
    }

}
