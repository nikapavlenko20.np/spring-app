package app.emun;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP;
    public static Currency getByName(String name){
        return Currency.valueOf(name);
    }
}
