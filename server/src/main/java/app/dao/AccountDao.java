package app.dao;

import app.entity.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountDao implements Dao<Account> {
    public static List<Account> list =  new ArrayList<>();
    private static long count = 0;

    @Override
    public Account save(Account obj) {
        boolean add = list.add(obj);
        return obj;
    }

    @Override
    public boolean delete(Account obj) {
        return list.remove(obj);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        list.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Account> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Account> first = list.stream().filter(x -> x.getId() == id).findFirst();
        return first.map(account -> list.remove(account)).orElse(false);
    }

    @Override
    public Account getOne(long id) {
        Optional<Account> first = list.stream().filter(x -> x.getId() == id).findFirst();
        return first.orElse(null);
    }
}
