package app.dao;

import app.entity.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CustomerDao implements Dao<Customer> {
   private List<Customer> list = new ArrayList<>();
    private static long count = 0;

    @Override
    public Customer save(Customer obj) {
        list.add(obj);
        return obj;
    }

    @Override
    public boolean delete(Customer obj) {
        return list.remove(obj);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        list.removeAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        list.addAll(entities);
    }

    @Override
    public List<Customer> findAll() {
        return list;
    }

    @Override
    public boolean deleteById(long id) {
        Optional<Customer> first = list.stream().filter(x -> x.getId() == id).findFirst();
        return first.map(customer -> list.remove(customer)).orElse(false);
    }

    @Override
    public Customer getOne(long id) {
        Optional<Customer> first = list.stream().filter(x -> x.getId() == id).findFirst();
        return first.orElse(null);
    }

}
