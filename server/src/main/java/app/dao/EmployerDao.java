package app.dao;

import app.entity.Employer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmployerDao implements Dao<Employer> {
    private ArrayList<Employer> employers = new ArrayList<>();
    private int count = 0;


    @Override
    public Employer save(Employer e) {
        boolean add = employers.add(e);
        return add ? e : null;
    }

    @Override
    public boolean delete(Employer employer) {
        return employers.remove(employer);
    }

    @Override
    public void deleteAll(List<Employer> entities) {
        employers.removeAll(entities);
    }

    @Override
    public void saveAll(List<Employer> entities) {
        employers.addAll(entities);
    }

    @Override
    public List<Employer> findAll() {
        return employers;
    }

    @Override
    public boolean deleteById(long id) {
        return employers.remove(id);
    }

    @Override
    public Employer getOne(long id) {
        Optional<Employer> first = employers.stream().filter(x -> x.getId() == id).findFirst();
        return first.orElse(null);
    }
}
