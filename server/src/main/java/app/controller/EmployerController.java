package app.controller;

import app.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("employers")
@RequiredArgsConstructor
public class EmployerController {
    private EmployerService employerService;
}
