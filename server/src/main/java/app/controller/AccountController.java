package app.controller;

import app.entity.Account;
import app.facade.AccountMapper;
import app.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountMapper accountMapper;

    @PatchMapping()
    public void topUpAccount(@RequestParam("money") Double money, @RequestParam("accountID") Long accountID) {
        accountMapper.topUpAccount(money, accountID);
    }

    @PatchMapping("cashOut")
    public boolean cashOutForAccount(@RequestParam("money") Double money, @RequestParam("accountID") Long accountID) {
       return accountMapper.cashOutForAccount(money, accountID);
    }

    @PatchMapping("transaction")
    public boolean transactionForAccounts(@RequestParam("money") Double money, @RequestParam("accountFromID") Long accountFromID, @RequestParam("accountToID") Long accountToID) {
       return accountMapper.transactionForAccounts(money, accountFromID, accountToID);
    }

}
