package app.controller;

import app.dto.request.AccountRequest;
import app.dto.request.CustomerRequest;
import app.dto.response.AccountResponse;
import app.dto.response.CustomerResponse;
import app.entity.Account;
import app.entity.Customer;
import app.facade.AccountMapper;
import app.facade.CustomerMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerMapper customerMapper;
    private final AccountMapper accountMapper;

    @PostMapping()
    public Customer createCustomer(@Validated @RequestBody CustomerRequest customer) {
        return customerMapper.createCustomer(customer);
    }

    @PutMapping("{id}")
    public Customer updateCustomer(@RequestBody CustomerRequest customer, @PathVariable("id") Long id) {
        return customerMapper.updateCustomer(customer, id);
    }

    @DeleteMapping("{id}")
    public boolean deleteCustomer(@PathVariable("id") Long id) {
        return customerMapper.deleteCustomer(id);
    }

    @GetMapping()
    public List<CustomerResponse> getCustomers() {
        return customerMapper.getAllCustomers();
    }

    @GetMapping("{id}")
    public CustomerResponse getCustomerById(@PathVariable("id") Long id) {
        System.out.println(id);
        System.out.println("getCustomerById");
        return customerMapper.getCustomerById(id);
    }

    @PostMapping("account/{id}")
    public Account postAccount(@RequestParam("currency") String currency, @PathVariable("id") Long id) {
        return customerMapper.createAccount(currency, id);
    }

    @DeleteMapping("account/{idAccount}/{idCustomer}")
    public void deleteAccount(@PathVariable("idAccount") Long idAccount) {
        accountMapper.deleteAccount(idAccount);
    }
}
