package app.service;

import app.dao.AccountDao;
import app.dao.CustomerDao;
import app.entity.Account;
import app.entity.Customer;
import app.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;

    public Optional<Account> getAccountById(Long id) {
        return accountRepository.findById(id);
    }

    public void deleteAccountById(Long id) {
        accountRepository.deleteById(id);
    }

    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }


}
