package app.service;

import app.entity.Employer;
import app.repository.EmployerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployerService {
    private final EmployerRepository employerRepository;

    public List<Employer> getAll() {
        return employerRepository.findAll();
    }

    public void delete(Long id) {
        employerRepository.deleteById(id);
    }

    public Employer getEmployerById(Long id) {
        return employerRepository.getOne(id);
    }

    public Employer createEmploye(Employer employer) {
        return employerRepository.save(employer);
    }
}
