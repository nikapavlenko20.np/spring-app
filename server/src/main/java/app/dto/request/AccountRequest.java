package app.dto.request;

import app.emun.Currency;
import lombok.Data;

import javax.validation.constraints.PositiveOrZero;

@Data
public class AccountRequest {
    private Currency currency;
    @PositiveOrZero(message = "balance must be positive")
    private Double balance = 0d;

}
