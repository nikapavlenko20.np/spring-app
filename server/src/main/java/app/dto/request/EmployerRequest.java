package app.dto.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class EmployerRequest {
    @NotBlank(message = "Name is required")
    @Size(min = 3, message="min length 3")
    private String name;

    @NotBlank(message = "Address is required")
    @Size(min = 3, message="min length 3")
    private String address;
}
