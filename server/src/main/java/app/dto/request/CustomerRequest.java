package app.dto.request;

import lombok.Data;
import javax.validation.constraints.*;

@Data
public class CustomerRequest {
    @NotBlank(message = "Mame is required")
    @Size(min = 2, message = "min length - 2 letters")
    private String name;

    @Pattern(regexp = "^([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})$",
            message = "Email ist valid")
    private String email;

    @Positive(message = "Age must be number min 18")
    @Min(18)
    private Integer age;

    @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$",
            message = "phone ist valid")
    private String phone;

    private String password;
}
