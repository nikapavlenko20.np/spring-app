package app.dto.response;

import app.entity.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

public class EmployerResponse {
    private Long id;
    private String name;
    private String address;
    private List<CustomerResponse> customers;
}
