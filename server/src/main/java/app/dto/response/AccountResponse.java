package app.dto.response;

import app.emun.Currency;
import app.entity.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.UUID;

public class AccountResponse {
    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private Customer customer;
}
