package app.dto.response;

import java.util.List;

public class CustomerResponse {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phone;
    private List<AccountResponse> accounts;
    private List<EmployerResponse> employers;
}
