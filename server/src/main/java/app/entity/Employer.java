package app.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@SuppressWarnings("ID")
@Entity
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Employer extends AbstractEntity<Employer> {
    private String name;
    private String address;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "employers_to_customers",
            joinColumns = @JoinColumn(name = "employer_id"),
            inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private List<Customer> customers = new ArrayList<>();


}
