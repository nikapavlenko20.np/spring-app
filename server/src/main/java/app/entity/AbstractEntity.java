package app.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("ID")
@MappedSuperclass
abstract public class AbstractEntity<T> {
    @Id
    @Setter
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreatedDate
    @Setter
    @Getter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedDate
    @Setter
    @Getter
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;
}
