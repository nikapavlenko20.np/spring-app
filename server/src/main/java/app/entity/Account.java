package app.entity;

import app.emun.Currency;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.UUID;

@Data
@SuppressWarnings("ID")
@Entity
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Account extends AbstractEntity<Account> {

    @Column(unique = true)
    private String number = UUID.randomUUID().toString();

    private Currency currency;

//    @Column(name = "a_balance")
    private Double balance = 0d;


//    @ManyToOne
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
    @JoinTable(name = "accounts_to_customer",
        joinColumns = @JoinColumn(name = "account_id"),
        inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private Customer customer;

}
